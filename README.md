## Dart File of class without edit

- This code is May be Incorrect
- Recommended don't use this version

> Hi, to get source code avaible two way<br>
> 1- Use `git clone ...` to get<br>
> 2- Download Compressed file

# Git

```sh
cd ${Parent_Directory}
git clone https://gitlab.com/fakharamirali/fluttercoderaw.git
cd FlutterCodeRaw
git checkout lesson${Your_lesson_Number}
```
# Download

| Lesson | Download Link |
| ------ | ------------- |
|   2    | [#lesson1][L1] |
|   3    | [#lesson2][L3] |
|   4    | [#lesson3][L4] |
|   5    | [#lesson4][L5] |
|   6    | [#lesson5][L6] |
|   7    | [#lesson6][L7] |
|   8    | [#lesson7][L8] |
|   9    | [#lesson8][L9] |
|   10    | [#lesson9][L10] |
|   11    | [#lesson10][L11] |
|  sample | [-sample][sample]|


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [L1]: <https://gitlab.com/fakharamirali/fluttercoderaw/-/archive/master/fluttercoderaw-master.zip>
   [L2]: <https://gitlab.com/fakharamirali/fluttercoderaw/-/archive/lesson2/fluttercoderaw-lesson2.zip>
   [L3]: <https://gitlab.com/fakharamirali/fluttercoderaw/-/archive/lesson3/fluttercoderaw-lesson3.zip>
   [L4]: <https://gitlab.com/fakharamirali/fluttercoderaw/-/archive/lesson4/fluttercoderaw-lesson4.zip>
   [L5]: <https://gitlab.com/fakharamirali/fluttercoderaw/-/archive/lesson5/fluttercoderaw-lesson5.zip>
   [L6]: <https://gitlab.com/fakharamirali/fluttercoderaw/-/archive/lesson6/fluttercoderaw-lesson6.zip>
   [L7]: <https://gitlab.com/fakharamirali/fluttercoderaw/-/archive/lesson7/fluttercoderaw-lesson7.zip>
   [L8]: <https://gitlab.com/fakharamirali/fluttercoderaw/-/archive/lesson8/fluttercoderaw-lesson8.zip>
   [L9]: <https://gitlab.com/fakharamirali/fluttercoderaw/-/archive/lesson9/fluttercoderaw-lesson9.zip>
   [L11]: <https://gitlab.com/fakharamirali/fluttercoderaw/-/archive/lesson1/fluttercoderaw-lesson1.zip>
   [L10]: <https://gitlab.com/fakharamirali/fluttercoderaw/-/archive/lesson10/fluttercoderaw-lesson10.zip> 
   [sample]: <https://gitlab.com/fakharamirali/fluttercoderaw/-/archive/sample/fluttercoderaw-sample.zip> 
